package ru.t1.nkiryukhin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.nkiryukhin.tm.api.service.*;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.nkiryukhin.tm.exception.entity.TaskNotFoundException;
import ru.t1.nkiryukhin.tm.exception.field.ProjectIdEmptyException;
import ru.t1.nkiryukhin.tm.exception.field.TaskIdEmptyException;
import ru.t1.nkiryukhin.tm.exception.field.UserIdEmptyException;
import ru.t1.nkiryukhin.tm.marker.UnitCategory;
import ru.t1.nkiryukhin.tm.model.Task;

import static ru.t1.nkiryukhin.tm.data.ProjectTestData.*;
import static ru.t1.nkiryukhin.tm.data.TaskTestData.*;
import static ru.t1.nkiryukhin.tm.data.UserTestData.USUAL_USER;

@Category(UnitCategory.class)
public final class ProjectTaskServiceTest {

    @NotNull static final IPropertyService propertyService = new PropertyService();

    @NotNull static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull static final ITaskService taskService = new TaskService(connectionService);

    @NotNull static final IProjectService projectService = new ProjectService(connectionService);

    @NotNull static final IProjectTaskService service = new ProjectTaskService(projectService, taskService);

    @Before
    public void before() {
        projectService.add(USUAL_PROJECT1);
        projectService.add(USUAL_PROJECT2);
        taskService.add(USUAL_TASK1);
        taskService.add(USUAL_TASK2);
    }

    @After
    public void after() throws AbstractException {
        taskService.removeAll(TASK_LIST);
        projectService.removeAll(PROJECT_LIST);
    }

    @Test
    public void bindTaskToProject() throws AbstractException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.bindTaskToProject(null, USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.bindTaskToProject("", USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.bindTaskToProject(USUAL_USER.getId(), null, USUAL_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.bindTaskToProject(USUAL_USER.getId(), "", USUAL_TASK1.getId());
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            service.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), null);
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            service.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.bindTaskToProject(USUAL_USER.getId(), NON_EXISTING_PROJECT_ID, USUAL_TASK1.getId());
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            service.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), NON_EXISTING_TASK_ID);
        });
        service.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT2.getId(), USUAL_TASK1.getId());
        Task task = taskService.findOneById(USUAL_USER.getId(), USUAL_TASK1.getId());
        Assert.assertEquals(USUAL_PROJECT2.getId(), task.getProjectId());
        service.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
    }

    @Test
    public void removeProjectById() throws AbstractException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeProjectById(null, USUAL_PROJECT1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeProjectById("", USUAL_PROJECT1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.removeProjectById(USUAL_USER.getId(), null);
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.removeProjectById(USUAL_USER.getId(), "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.removeProjectById(USUAL_USER.getId(), NON_EXISTING_PROJECT_ID);
        });
        service.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
        service.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), USUAL_TASK2.getId());
        service.removeProjectById(USUAL_USER.getId(), USUAL_PROJECT1.getId());
        Assert.assertNull(projectService.findOneById(USUAL_PROJECT1.getId()));
        Assert.assertNull(taskService.findOneById(USUAL_TASK1.getId()));
        Assert.assertNull(taskService.findOneById(USUAL_TASK2.getId()));
    }

    @Test
    public void unbindTaskFromProject() throws AbstractException {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.unbindTaskFromProject(null, USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.unbindTaskFromProject("", USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.unbindTaskFromProject(USUAL_USER.getId(), null, USUAL_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.unbindTaskFromProject(USUAL_USER.getId(), "", USUAL_TASK1.getId());
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            service.unbindTaskFromProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), null);
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            service.unbindTaskFromProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.unbindTaskFromProject(USUAL_USER.getId(), NON_EXISTING_PROJECT_ID, USUAL_TASK1.getId());
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            service.unbindTaskFromProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), NON_EXISTING_TASK_ID);
        });
        service.unbindTaskFromProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
        Task task = taskService.findOneById(USUAL_USER.getId(), USUAL_TASK1.getId());
        Assert.assertNull(task.getProjectId());
        service.bindTaskToProject(USUAL_USER.getId(), USUAL_PROJECT1.getId(), USUAL_TASK1.getId());
    }

}

