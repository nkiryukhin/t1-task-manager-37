package ru.t1.nkiryukhin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.model.Project;


public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @NotNull
    Project create(@Nullable String UserId, @NotNull String name, @NotNull String description);

    @NotNull
    Project create(@Nullable String UserId, @NotNull String name);

    @NotNull
    Project update(@NotNull Project project);

}