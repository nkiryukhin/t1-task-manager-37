package ru.t1.nkiryukhin.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.api.repository.IUserOwnedRepository;
import ru.t1.nkiryukhin.tm.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Nullable
    @Override
    public abstract M add(@Nullable final String userId, @Nullable final M model);

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return findOneById(userId, id) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@NotNull final String userId) {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE user_id = ?", getTableName());
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            while (rowSet.next()) result.add(fetch(rowSet));
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@NotNull final String userId, @Nullable final Comparator comparator) {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE user_id = ? ORDER BY %s", getTableName(), getSortType(comparator)
        );
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            while (rowSet.next()) result.add(fetch(rowSet));
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE id = ? AND user_id = ? LIMIT 1", getTableName());
        try(@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            statement.setString(2, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE user_id = ? LIMIT ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setInt(2, index + 1);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            for (int i = 1; i <= index; i++) {
                rowSet.next();
            }
            return fetch(rowSet);
        }
    }

    @Override
    @SneakyThrows
    public int getSize(@NotNull final String userId) {
        @NotNull final String sql = String.format("SELECT COUNT(1) FROM %s WHERE user_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getInt("count");
        }
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return null;
        return removeOne(model);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || index == null) return null;
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return removeOne(model);
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        @NotNull List<M> list = findAll(userId);
        removeAll(list);
    }

}
