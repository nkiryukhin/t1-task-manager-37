package ru.t1.nkiryukhin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.model.User;


public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    @Nullable
    Boolean isLoginExist(@Nullable String login);

    @Nullable
    Boolean isEmailExist(@Nullable String email);

    void update(@NotNull User user);

}