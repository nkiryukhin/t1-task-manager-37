package ru.t1.nkiryukhin.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.api.repository.ITaskRepository;
import ru.t1.nkiryukhin.tm.enumerated.Status;
import ru.t1.nkiryukhin.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    private static final String table = "tm_task";

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    protected String getTableName() {
        return table;
    }

    @NotNull
    @Override
    @SneakyThrows
    protected Task fetch(@NotNull ResultSet row) {
        @NotNull final Task task = new Task();
        task.setId(row.getString("id"));
        task.setName(row.getString("name"));
        task.setDescription(row.getString("description"));
        task.setStatus(Status.toStatus(row.getString("status")));
        task.setCreated(row.getTimestamp("created"));
        task.setUserId(row.getString("user_id"));
        task.setProjectId(row.getString("project_id"));
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task add(@NotNull final Task task) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (id, created, name, description, status, user_id, project_id) " +
                        "VALUES (?, ?, ?, ?, ?::status, ?, ?)",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getId());
            statement.setTimestamp(2, new Timestamp(task.getCreated().getTime()));
            statement.setString(3, task.getName());
            statement.setString(4, task.getDescription());
            statement.setString(5, Status.NOT_STARTED.name());
            statement.setString(6, task.getUserId());
            statement.setString(7, task.getProjectId());
            statement.executeUpdate();
        }
        return task;
    }

    @Nullable
    @Override
    public Task add(@Nullable final String userId, @Nullable final Task task) {
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public Task create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public Task create(
            @NotNull final String userId,
            @NotNull final String name
    ) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String sql = String.format(
                "SELECT id, created, name, description, status, user_id, project_id" +
                        " FROM %s WHERE user_id = ? AND project_id = ?", getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, projectId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            while (rowSet.next()) result.add(fetch(rowSet));
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task update(@NotNull final Task task) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET name = ?, description = ?, status = ?::status, project_id = ? WHERE id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getName());
            statement.setString(2, task.getDescription());
            statement.setString(3, task.getStatus().name());
            statement.setString(4, task.getProjectId());
            statement.setString(5, task.getId());
            statement.executeUpdate();
        }
        return task;
    }

}
