package ru.t1.nkiryukhin.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.api.model.IWBS;
import ru.t1.nkiryukhin.tm.enumerated.Status;

import java.util.Date;
import java.util.Objects;

@Getter
@Setter
public final class Task extends AbstractUserOwnedModel implements IWBS {

    private static final long serialVersionUID = 1;

    @NotNull
    private Date created = new Date();

    @NotNull
    private String description = "";

    @NotNull
    private String name = "";

    @Nullable
    private String projectId;

    @NotNull
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description + " : " + status.getDisplayName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return created.equals(task.created)
                && name.equals(task.name)
                && Objects.equals(projectId, task.projectId)
                && status == task.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, projectId);
    }

}
