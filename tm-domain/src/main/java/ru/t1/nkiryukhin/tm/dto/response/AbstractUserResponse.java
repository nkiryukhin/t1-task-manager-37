package ru.t1.nkiryukhin.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public class AbstractUserResponse extends AbstractResponse {

    @Nullable
    private User user;

    public AbstractUserResponse(@Nullable User user) {
        this.user = user;
    }

}
